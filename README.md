# Indice delle classi con descrizione

## Classi relative all'albero sintattico
 - ASTNode: generico nodo con la possibilità di essere visitato da un NodeVisitor e un NodeTransformer
 - Program: programma, una sequenza di istruzioni

### Espressioni
 - Expression: classe astratta genitore di tutte le espressioni
 - IntExpr: intero
 - VarExpr: variabile
 - NegExpr: negazione
 - ModExpr: modulo
 - SubExpr: sottrazione
 - AddExpr: addizione
 - DivExpr: divisione intera
 - MulExpr: moltiplicazione
 - TernaryExpr: operatore ternario
 - AssignmentExpr: assegnamento, assegna il valore alla variabile e lo mette in cima allo stack
 - InputExpr: input

### Istruzioni
 - Instruction: interfaccia genitore di tutte le istruzioni
 - OutputInstruction: output
 - AssignmentInstruction: assegnamento, assegna il valore alla variabile e *NON* lo mette in cima allo stack
 - EmptyInstruction: l'istruzione vuota (corrisponde ad una riga vuota o una riga contenente solo un commento)
 - LoopInstruction: ciclo con condizione e corpo
 - ErroneousInstruction: istruzione errata generata in caso di errore di parsing

## Analisi Lessicalie e Sintattica
 - lexer.jflex: lexer implementato con JFlex
 - EchoLexer: classe con main di prova per il lexer
 - parser.cup: parser implementato con CUP

## Analisi Semantica

### Visita albero sintattico
 - NodeVisitor: interfaccia base per la visita dell'AST senza valore di ritorno
 - NodeTransformer: interfaccia base per la visita e modifica dell'AST, ritorna in un nuovo AST il risultato della trasformazione
 - CloneTransformer: implementa NodeTransformer, clona un AST
 - RemoveMod: rimuove le operazioni di modulo e le rimpiazza con una espressione equivalente basate sulle altre (a % b diventa ((r0 = a) - (r0 / (r0 = b)) * r0))
 - UninitializedVarAnalysis: identifica e notifica gli utilizzi di variabili non inizializzate

## Comuni alle fasi
 - SymbolTable: tabella dei simboli, contiene una mappa che associa il nome di una variabile con il suo descrittore
 - VarDescriptor: descrittore di una variabile, contiene l'identificativo e l'indirizzo della variabile

## Generazione di Codice
 - CodeGenerator: genera codice per la Macchina Stack visitando l'AST privo di operazioni di modulo o operazioni erronee
 - Compiler: effettua tutte le fasi di compilazione per un dato file o InputStream
 - CompilationFailedException: eccezione che notifica il fallimento della compilazione

## Diagnostica
 - Diagnostics: genera i messaggi di errori per le varie fasi
 - LineBuffer: interfaccia di un buffer da cui è possibile ottenere le riga di un file o dell'input
 - LineBufferedInputStream: incapsula un InputStream e memorizza le linee durante la lettura per uso successivo

## Altre
 - Main: compila il file passato come primo argomento opure lo standard input se nessun argomento è presente


# Guida utilizzo

Per compilare un file sorgente, ad esempio `examples/min.prg`, eseguire il seguente comando:
```bash
$ gradle --console plain run -Pargs=examples/min.prg
```

Questo comando oltre ad effettuare la compilazione, esegue anche l'eseguibile risultante ed inoltre crea due file nella cartella di lavoro:
 - `a.out`: eseguibile risultante dalla compilazione
 - `a.asm`: decompilato dell'eseguibile in forma leggibile
