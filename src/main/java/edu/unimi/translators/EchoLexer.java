package edu.unimi.translators;

import edu.unimi.translators.lexing.CompilerLexer;
import edu.unimi.translators.parsing.CompilerParserSym;
import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.Symbol;

import java.io.IOException;
import java.io.InputStreamReader;

public class EchoLexer {
	public static void main(String[] args) throws IOException {
		CompilerLexer lexer = new CompilerLexer(new InputStreamReader(System.in), new ComplexSymbolFactory());

		Symbol symbol;
		do {
			symbol = lexer.next_token();
			System.out.println(symbol.toString());
		} while (symbol.sym != CompilerParserSym.EOF);
	}
}
