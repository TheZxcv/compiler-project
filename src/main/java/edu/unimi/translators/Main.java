package edu.unimi.translators;

import edu.unimi.translators.compiler.CompilationFailedException;
import edu.unimi.translators.compiler.Compiler;
import lt.macchina.Macchina;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Main {
	public static void main(String[] args) throws Exception {
		Compiler compiler;
		if (args.length > 0) {
			compiler = new Compiler(args[0]);
		} else {
			compiler = new Compiler(System.in);
		}

		try {
			File executable = compiler.compile();
			Macchina stackMachine = new Macchina(executable.getAbsolutePath());
			saveDisassembly(executable, stackMachine);
			stackMachine.esegui();
		} catch (CompilationFailedException e) {
			System.err.println("Compilation failed.");
			System.exit(-1);
		}
	}

	private static void saveDisassembly(File executable, Macchina stackMachine) throws FileNotFoundException {
		String filename = executable.getName().replace(".out", ".asm");

		try (PrintWriter out = new PrintWriter(filename)) {
			out.print(stackMachine.toString());
		}
	}
}
