package edu.unimi.translators.transformers;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.expressions.*;
import edu.unimi.translators.compiler.SymbolTable;

public class RemoveMod extends CloneTransformer {

	private final SymbolTable symbolTable;
	private final String tempReg;

	public RemoveMod(SymbolTable symbolTable, String tempReg) {
		this.symbolTable = symbolTable;
		this.tempReg = tempReg;
	}

	public RemoveMod(SymbolTable symbolTable) {
		this(symbolTable, "0");
	}

	@Override
	public ASTNode visit(ModExpr node) {
		symbolTable.put(tempReg);
		return new SubExpr(
			new AssignmentExpr(tempReg, (Expression) node.left.visit(this)),
			new MulExpr(
				new DivExpr(
					new VarExpr(tempReg),
					new AssignmentExpr(tempReg, (Expression) node.right.visit(this))
				),
				new VarExpr(tempReg)
			)
		);
	}

}
