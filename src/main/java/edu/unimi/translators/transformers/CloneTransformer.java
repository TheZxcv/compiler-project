package edu.unimi.translators.transformers;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.expressions.*;
import edu.unimi.translators.ast.instructions.*;

import java.util.ArrayList;
import java.util.List;

public abstract class CloneTransformer implements NodeTransformer {

	public Program transform(Program prog) {
		return (Program) visit(prog);
	}

	@Override
	public ASTNode visit(Program node) {
		List<Instruction> instrs = new ArrayList<>();
		for (ASTNode c : node.instrs)
			instrs.add((Instruction) c.visit(this));
		return new Program(instrs);
	}

	@Override
	public ASTNode visit(LoopInstruction node) {
		List<Instruction> instrs = new ArrayList<>();
		Expression cond = (Expression) node.cond.visit(this);
		for (ASTNode c : node.body)
			instrs.add((Instruction) c.visit(this));
		return new LoopInstruction(cond, instrs);
	}

	@Override
	public ASTNode visit(AssignmentInstruction node) {
		return new AssignmentInstruction(node.ident, (Expression) node.expr.visit(this));
	}

	@Override
	public ASTNode visit(OutputInstruction node) {
		if (node.expr != null)
			return new OutputInstruction(node.message, (Expression) node.expr.visit(this));
		else
			return new OutputInstruction(node.message);
	}

	@Override
	public ASTNode visit(AddExpr node) {
		return new AddExpr((Expression) node.left.visit(this), (Expression) node.right.visit(this));
	}

	@Override
	public ASTNode visit(DivExpr node) {
		return new DivExpr((Expression) node.left.visit(this), (Expression) node.right.visit(this));
	}

	@Override
	public ASTNode visit(InputExpr node) {
		return new InputExpr(node.message);
	}

	@Override
	public ASTNode visit(IntExpr node) {
		return new IntExpr(node.value);
	}

	@Override
	public ASTNode visit(ModExpr node) {
		return new ModExpr((Expression) node.left.visit(this), (Expression) node.right.visit(this));
	}

	@Override
	public ASTNode visit(MulExpr node) {
		return new MulExpr((Expression) node.left.visit(this), (Expression) node.right.visit(this));
	}

	@Override
	public ASTNode visit(NegExpr node) {
		return new NegExpr((Expression) node.expr.visit(this));
	}

	@Override
	public ASTNode visit(SubExpr node) {
		return new SubExpr((Expression) node.left.visit(this), (Expression) node.right.visit(this));
	}

	@Override
	public ASTNode visit(TernaryExpr node) {
		return new TernaryExpr(
			(Expression) node.cond.visit(this),
			(Expression) node.ethen.visit(this),
			(Expression) node.eelse.visit(this)
		);
	}

	@Override
	public ASTNode visit(AssignmentExpr node) {
		return new AssignmentExpr(node.ident, (Expression) node.expr.visit(this));
	}

	@Override
	public ASTNode visit(VarExpr node) {
		return new VarExpr(node.ident);
	}

	@Override
	public ASTNode visit(EmptyInstruction node) {
		return new EmptyInstruction();
	}

}
