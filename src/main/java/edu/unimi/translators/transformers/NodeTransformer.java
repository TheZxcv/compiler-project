package edu.unimi.translators.transformers;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.expressions.*;
import edu.unimi.translators.ast.instructions.*;

public interface NodeTransformer {
	ASTNode visit(Program node);

	ASTNode visit(LoopInstruction node);

	ASTNode visit(AssignmentInstruction node);

	ASTNode visit(OutputInstruction node);

	ASTNode visit(AddExpr node);

	ASTNode visit(DivExpr node);

	ASTNode visit(InputExpr node);

	ASTNode visit(IntExpr node);

	ASTNode visit(ModExpr node);

	ASTNode visit(MulExpr node);

	ASTNode visit(NegExpr node);

	ASTNode visit(SubExpr node);

	ASTNode visit(TernaryExpr node);

	ASTNode visit(AssignmentExpr node);

	ASTNode visit(VarExpr node);

	ASTNode visit(EmptyInstruction node);
}
