package edu.unimi.translators.diagnostics;

import edu.unimi.translators.buffer.LineBuffer;
import java_cup.runtime.ComplexSymbolFactory;

import static java.lang.System.lineSeparator;

public class Diagnostics {
	private static final String EXPANDED_TAB = "    ";
	private static final int TAB_WIDTH = EXPANDED_TAB.length();
	private static final String SYNTAX_MESSAGE_FORMAT = "%s (line=%d, col=%d) got %s";
	private static final String LEXING_MESSAGE_FORMAT = "Illegal character (line=%d, col=%d) got <%s>";

	private final LineBuffer buffer;

	public Diagnostics(final LineBuffer buffer) {
		this.buffer = buffer;
	}

	public String createSyntaxErrorMessage(String message, ComplexSymbolFactory.ComplexSymbol symbol) {
		StringBuilder sb = new StringBuilder();
		int lineno = symbol.getLeft().getLine();
		int col_off = symbol.getLeft().getColumn();

		sb.append(String.format(SYNTAX_MESSAGE_FORMAT, message, lineno, col_off, symbol.getName()))
			.append(lineSeparator())
			.append(makeLineWithMarker(lineno, col_off));
		return sb.toString();
	}

	public String createLexingErrorMessage(String erroneousInput, int lineno, int col_off) {
		StringBuilder sb = new StringBuilder();

		sb.append(String.format(LEXING_MESSAGE_FORMAT, lineno, col_off, erroneousInput))
			.append(lineSeparator())
			.append(makeLineWithMarker(lineno, col_off));
		return sb.toString();
	}

	public String createUninitializedErrorMessage(final String ident) {
		return String.format("Error: use of \"%s\" before initialization", ident);
	}

	private String makeLineWithMarker(int lineno, int col_off) {
		String line = buffer.getLine(lineno);
		int markerOffset = calculateOffset(line, col_off);
		String header = String.format("line %d: ", lineno);

		StringBuilder sb = new StringBuilder();
		sb.append(header)
			.append(expandTabs(line))
			.append(lineSeparator());
		for (int i = 0; i < markerOffset + header.length() - 1; i++)
			sb.append(" ");
		sb.append("^");
		return sb.toString();
	}

	private int calculateOffset(String line, int ncol) {
		int count = 0;
		int upper = Math.min(ncol, line.length());
		for (int i = 0; i < upper; i++) {
			if (line.charAt(i) != '\t')
				count++;
			else
				count += TAB_WIDTH;
		}
		return count + (ncol - upper);
	}

	private String expandTabs(String line) {
		StringBuilder sb = new StringBuilder();

		for (char c : line.toCharArray()) {
			if (c != '\t')
				sb.append(c);
			else
				sb.append(EXPANDED_TAB);
		}
		return sb.toString();
	}
}
