package edu.unimi.translators.ast;

import edu.unimi.translators.transformers.NodeTransformer;

public interface ASTNode {
	void visit(NodeVisitor visitor);
	ASTNode visit(NodeTransformer visitor);
}
