package edu.unimi.translators.ast.instructions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.transformers.NodeTransformer;

public class ErroneousInstruction implements Instruction {
	@Override
	public String toString() {
		return "ErroneousInstruction{}";
	}

	@Override
	public void visit(NodeVisitor visitor) {

	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return this;
	}
}
