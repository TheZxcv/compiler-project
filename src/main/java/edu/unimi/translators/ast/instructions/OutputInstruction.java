package edu.unimi.translators.ast.instructions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.ast.expressions.Expression;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.Objects;

public class OutputInstruction implements Instruction {

	public final String message;
	public final Expression expr;

	public OutputInstruction(final String message, Expression expr) {
		this.message = message;
		this.expr = expr;
	}

	public OutputInstruction(final String message) {
		this(message, null);
	}

	public OutputInstruction(final Expression expr) {
		this(null, expr);
	}

	@Override
	public String toString() {
		return "OutputInstruction{" +
			"message='" + message + '\'' +
			"expr=" + expr + "'}";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		OutputInstruction that = (OutputInstruction) o;

		if (message != null ? !message.equals(that.message) : that.message != null) return false;
		return expr != null ? expr.equals(that.expr) : that.expr == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(message, expr);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
