package edu.unimi.translators.ast.instructions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Program implements ASTNode {
	public final List<Instruction> instrs;

	public Program(final List<Instruction> instrs) {
		this.instrs = instrs != null ? instrs : Collections.emptyList();
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Program {");
		for (Instruction i : instrs)
			sb.append(System.lineSeparator())
				.append('\t')
				.append(i);
		sb.append(System.lineSeparator()).append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Program program = (Program) o;

		return instrs != null ? instrs.equals(program.instrs) : program.instrs == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(instrs);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
