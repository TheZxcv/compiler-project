package edu.unimi.translators.ast.instructions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.ast.expressions.Expression;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.List;
import java.util.Objects;

public class LoopInstruction implements Instruction {
	public final Expression cond;
	public final List<Instruction> body;

	public LoopInstruction(final Expression cond, final List<Instruction> body) {
		this.cond = cond;
		this.body = body;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("\tLoopInstruction{cond=");
		sb.append(cond);
		for (Instruction i : body)
			sb.append(System.lineSeparator())
				.append("\t\t")
				.append(i.toString());
		sb.append(System.lineSeparator())
			.append('\t').append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		LoopInstruction that = (LoopInstruction) o;

		if (cond != null ? !cond.equals(that.cond) : that.cond != null) return false;
		return body != null ? body.equals(that.body) : that.body == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cond, body);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
