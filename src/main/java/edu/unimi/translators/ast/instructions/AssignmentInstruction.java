package edu.unimi.translators.ast.instructions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.ast.expressions.Expression;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.Objects;

public class AssignmentInstruction implements Instruction {
	public final String ident;
	public final Expression expr;

	public AssignmentInstruction(final String ident, final Expression expr) {
		this.ident = ident;
		this.expr = expr;
	}

	@Override
	public String toString() {
		return "AssignmentInstruction{" +
			"ident='" + ident + '\'' +
			", expr=" + expr +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AssignmentInstruction that = (AssignmentInstruction) o;

		if (ident != null ? !ident.equals(that.ident) : that.ident != null) return false;
		return expr != null ? expr.equals(that.expr) : that.expr == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ident, expr);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
