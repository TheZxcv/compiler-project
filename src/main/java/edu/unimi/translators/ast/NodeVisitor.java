package edu.unimi.translators.ast;

import edu.unimi.translators.ast.expressions.*;
import edu.unimi.translators.ast.instructions.*;

public interface NodeVisitor {
	void visit(Program node);

	void visit(LoopInstruction node);

	void visit(AssignmentInstruction node);

	void visit(OutputInstruction node);

	void visit(AddExpr node);

	void visit(DivExpr node);

	void visit(InputExpr node);

	void visit(IntExpr node);

	void visit(ModExpr node);

	void visit(MulExpr node);

	void visit(NegExpr node);

	void visit(SubExpr node);

	void visit(TernaryExpr node);

	void visit(AssignmentExpr node);

	void visit(VarExpr node);

	void visit(EmptyInstruction node);
}
