package edu.unimi.translators.ast.expressions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.Objects;

public class InputExpr extends Expression {
	public final String message;

	public InputExpr() {
		this(null);
	}

	public InputExpr(final String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "InputExpr{" +
			"message='" + message + '\'' +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		InputExpr inputExpr = (InputExpr) o;

		return message != null ? message.equals(inputExpr.message) : inputExpr.message == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(message);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
