package edu.unimi.translators.ast.expressions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.Objects;

public class TernaryExpr extends Expression {
	public final Expression cond;
	public final Expression ethen;
	public final Expression eelse;

	public TernaryExpr(final Expression cond, final Expression ethen, final Expression eelse) {
		this.cond = cond;
		this.ethen = ethen;
		this.eelse = eelse;
	}

	@Override
	public String toString() {
		return "TernaryExpr{" +
			"cond=" + cond +
			", ethen=" + ethen +
			", eelse=" + eelse +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TernaryExpr that = (TernaryExpr) o;

		if (cond != null ? !cond.equals(that.cond) : that.cond != null) return false;
		if (ethen != null ? !ethen.equals(that.ethen) : that.ethen != null) return false;
		return eelse != null ? eelse.equals(that.eelse) : that.eelse == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cond, ethen, eelse);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
