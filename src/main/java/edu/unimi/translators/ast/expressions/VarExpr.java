package edu.unimi.translators.ast.expressions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.Objects;

public class VarExpr extends Expression {
	public final String ident;

	public VarExpr(final String id) {
		this.ident = id;
	}

	@Override
	public String toString() {
		return "VarExpr{" +
			"ident='" + ident + '\'' +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		VarExpr varExpr = (VarExpr) o;

		return ident != null ? ident.equals(varExpr.ident) : varExpr.ident == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ident);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
