package edu.unimi.translators.ast.expressions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.Objects;

public class AddExpr extends Expression {
	public final Expression left;
	public final Expression right;

	public AddExpr(Expression e1, Expression e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public String toString() {
		return "AddExpr{" +
			"left=" + left +
			", right=" + right +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AddExpr addExpr = (AddExpr) o;

		if (left != null ? !left.equals(addExpr.left) : addExpr.left != null) return false;
		return right != null ? right.equals(addExpr.right) : addExpr.right == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, right);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
