package edu.unimi.translators.ast.expressions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.Objects;

public class ModExpr extends Expression {
	public final Expression left;
	public final Expression right;

	public ModExpr(final Expression e1, final Expression e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public String toString() {
		return "ModExpr{" +
			"left=" + left +
			", right=" + right +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ModExpr modExpr = (ModExpr) o;

		if (left != null ? !left.equals(modExpr.left) : modExpr.left != null) return false;
		return right != null ? right.equals(modExpr.right) : modExpr.right == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, right);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
