package edu.unimi.translators.ast.expressions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.transformers.NodeTransformer;

public class IntExpr extends Expression {
	public final int value;

	public IntExpr(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "IntExpr{" +
			"value=" + value +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		IntExpr intExpr = (IntExpr) o;

		return value == intExpr.value;
	}

	@Override
	public int hashCode() {
		return value;
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
