package edu.unimi.translators.ast.expressions;

import edu.unimi.translators.ast.ASTNode;
import edu.unimi.translators.ast.NodeVisitor;
import edu.unimi.translators.transformers.NodeTransformer;

import java.util.Objects;

public class NegExpr extends Expression {
	public final Expression expr;

	public NegExpr(final Expression e) {
		this.expr = e;
	}

	@Override
	public String toString() {
		return "NegExpr{" +
			"expr=" + expr +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NegExpr negExpr = (NegExpr) o;

		return expr != null ? expr.equals(negExpr.expr) : negExpr.expr == null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(expr);
	}

	@Override
	public void visit(NodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ASTNode visit(NodeTransformer transformer) {
		return transformer.visit(this);
	}
}
