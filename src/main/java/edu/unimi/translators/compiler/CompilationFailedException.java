package edu.unimi.translators.compiler;

public class CompilationFailedException extends RuntimeException {

	public CompilationFailedException() {
	}

	public CompilationFailedException(Exception e) {
		super(e);
	}
}
