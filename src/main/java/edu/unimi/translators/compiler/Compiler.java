package edu.unimi.translators.compiler;

import edu.unimi.translators.analysis.UninitializedVarAnalysis;
import edu.unimi.translators.ast.instructions.Program;
import edu.unimi.translators.buffer.LineBufferedInputStream;
import edu.unimi.translators.codegen.CodeGenerator;
import edu.unimi.translators.diagnostics.Diagnostics;
import edu.unimi.translators.lexing.CompilerLexer;
import edu.unimi.translators.parsing.CompilerParser;
import edu.unimi.translators.transformers.RemoveMod;
import java_cup.runtime.ComplexSymbolFactory;
import lt.macchina.Codice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class Compiler {
	private static final String DEFAULT_OUTPUT = "a.out";

	private final LineBufferedInputStream input;

	private SymbolTable symbolTable;
	private UninitializedVarAnalysis analysis;
	private Diagnostics diagnostics;

	public Compiler(final InputStream inputStream) {
		this.input = new LineBufferedInputStream(inputStream);
		this.diagnostics = new Diagnostics(input);
		this.analysis = new UninitializedVarAnalysis(diagnostics);
	}

	public Compiler(final File sourceFile) throws FileNotFoundException {
		this(new FileInputStream(sourceFile));
	}

	public Compiler(final String fileName) throws FileNotFoundException {
		this(new File(fileName));
	}

	public File compile() throws Exception {
		return compile(DEFAULT_OUTPUT);
	}

	public File compile(String outputFile) throws Exception {
		Reader reader = new InputStreamReader(input);

		ComplexSymbolFactory symbolFactory = new ComplexSymbolFactory();
		CompilerLexer lexer = new CompilerLexer(reader, symbolFactory, diagnostics);

		CompilerParser parser = new CompilerParser(lexer, symbolFactory);
		parser.diagnostics = diagnostics;

		Program prog;
		try {
			prog = (Program) parser.parse().value;
		} catch (Exception e) {
			throw new CompilationFailedException(e);
		}
		if (parser.successfulParsing() && analyse(prog)) {
			this.symbolTable = parser.getSymbolTable();

			Program progWithoutMod = transform(prog);
			CodeGenerator codeGenerator = new CodeGenerator(symbolTable, new Codice(outputFile));
			codeGenerator.gencode(progWithoutMod);
			return new File(outputFile);
		} else {
			throw new CompilationFailedException();
		}
	}

	private Program transform(Program prog) {
		return new RemoveMod(symbolTable).transform(prog);
	}

	private boolean analyse(Program prog) {
		return analysis.analyze(prog);
	}
}
