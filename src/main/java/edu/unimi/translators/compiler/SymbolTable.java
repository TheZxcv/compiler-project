package edu.unimi.translators.compiler;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class SymbolTable {
	private Map<String, VarDescriptor> table;

	public SymbolTable() {
		table = new HashMap<>();
	}

	public void put(String ident) {
		table.putIfAbsent(ident, new VarDescriptor(ident));
	}

	public VarDescriptor get(String ident) {
		return table.get(ident);
	}

	public Collection<VarDescriptor> descriptors() {
		return table.values();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("SymbolTable {");
		for (String id : table.keySet()) {
			sb.append(System.lineSeparator())
				.append(id);
		}
		sb.append('}');
		return sb.toString();
	}
}
