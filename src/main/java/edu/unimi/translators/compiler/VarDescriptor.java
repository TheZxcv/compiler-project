package edu.unimi.translators.compiler;

public class VarDescriptor {
	private final String ident;
	private int address;

	public VarDescriptor(final String ident) {
		this.ident = ident;
	}

	public String getIdentifier() {
		return ident;
	}

	public int assignAddress(int address) {
		this.address = address;
		return address + 1;
	}

	public int getAddress() {
		return address;
	}
}
