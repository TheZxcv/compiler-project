package edu.unimi.translators.buffer;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class LineBufferedInputStream extends InputStream implements LineBuffer {
	private final List<String> lines;
	private int lastLine;
	private final InputStream stream;

	public LineBufferedInputStream(final InputStream stream) {
		this.lines = new ArrayList<>(1024);
		lines.add("");
		this.lastLine = 0;
		this.stream = stream;
	}

	@Override
	public int read() throws IOException {
		int data = this.stream.read();
		if (data != -1) {
			// FIXME: fragile
			if (data != '\n') {
				lines.set(lastLine, lines.get(lastLine) + (char) data);
			} else {
				lastLine++;
				lines.add("");
			}
		}
		return data;
	}

	@Override
	public String getLine(int line) {
		if (line <= lines.size())
			return lines.get(line - 1);
		else
			return "";
	}
}
