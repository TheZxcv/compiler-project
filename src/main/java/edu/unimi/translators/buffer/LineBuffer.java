package edu.unimi.translators.buffer;

public interface LineBuffer {
	String getLine(int line);
}
