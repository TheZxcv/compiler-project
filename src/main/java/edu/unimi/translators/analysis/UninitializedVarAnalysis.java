package edu.unimi.translators.analysis;

import edu.unimi.translators.ast.*;
import edu.unimi.translators.ast.expressions.*;
import edu.unimi.translators.ast.instructions.*;
import edu.unimi.translators.diagnostics.Diagnostics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UninitializedVarAnalysis implements NodeVisitor {

	private final Diagnostics diagnostics;
	private boolean failed = false;

	private final List<Set<String>> initializedScope = new ArrayList<>(1);

	public UninitializedVarAnalysis(final Diagnostics diagnostics) {
		this.diagnostics = diagnostics;
	}

	public UninitializedVarAnalysis() {
		this(null);
	}

	public boolean analyze(Program prog) {
		initializedScope.clear();
		visit(prog);
		return !failed;
	}

	private void pushScope() {
		initializedScope.add(new HashSet<>());
	}

	private void popScope() {
		initializedScope.remove(this.initializedScope.size() - 1);
	}

	private void addToScope(String ident) {
		initializedScope.get(this.initializedScope.size() - 1).add(ident);
	}

	private boolean assertInitialized(String ident) {
		for (int i = initializedScope.size() - 1; i >= 0; i--)
			if (initializedScope.get(i).contains(ident))
				return true;
		if (diagnostics != null)
			System.err.println(diagnostics.createUninitializedErrorMessage(ident));
		failed = true;
		return false;
	}


	@Override
	public void visit(Program node) {
		pushScope();
		for (ASTNode c : node.instrs)
			c.visit(this);
		popScope();
	}

	@Override
	public void visit(LoopInstruction node) {
		node.cond.visit(this);
		pushScope();
		for (ASTNode c : node.body)
			c.visit(this);
		popScope();
	}

	@Override
	public void visit(AssignmentInstruction node) {
		node.expr.visit(this);
		addToScope(node.ident);
	}

	@Override
	public void visit(OutputInstruction node) {
		if (node.expr != null)
			node.expr.visit(this);
	}

	@Override
	public void visit(AddExpr node) {
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(DivExpr node) {
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(InputExpr node) {

	}

	@Override
	public void visit(IntExpr node) {

	}

	@Override
	public void visit(ModExpr node) {
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(MulExpr node) {
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(NegExpr node) {
		node.expr.visit(this);
	}

	@Override
	public void visit(SubExpr node) {
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(TernaryExpr node) {
		node.cond.visit(this);

		pushScope();
		node.eelse.visit(this);
		popScope();

		pushScope();
		node.ethen.visit(this);
		popScope();
	}

	@Override
	public void visit(AssignmentExpr node) {
		node.expr.visit(this);
		addToScope(node.ident);
	}

	@Override
	public void visit(VarExpr node) {
		assertInitialized(node.ident);
	}

	@Override
	public void visit(EmptyInstruction node) {

	}
}
