package edu.unimi.translators.codegen;

import edu.unimi.translators.ast.*;
import edu.unimi.translators.ast.expressions.*;
import edu.unimi.translators.ast.instructions.*;
import edu.unimi.translators.compiler.SymbolTable;
import edu.unimi.translators.compiler.VarDescriptor;
import lt.macchina.Codice;

import java.io.IOException;

import static lt.macchina.Macchina.*;

public class CodeGenerator implements NodeVisitor {

	private final SymbolTable table;
	private final Codice codeGen;

	public CodeGenerator(final SymbolTable table, final Codice codeGen) {
		this.table = table;
		this.codeGen = codeGen;
	}

	public void gencode(Program prog) {
		prog.visit(this);
	}

	@Override
	public void visit(Program node) throws RuntimeException {
		allocateVariables();
		for (Instruction instr : node.instrs)
			instr.visit(this);
		codeGen.genera(HALT);

		try {
			codeGen.fineCodice();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void allocateVariables() {
		int firstFreeAddress = 0;
		for (VarDescriptor descr : table.descriptors()) {
			firstFreeAddress = descr.assignAddress(firstFreeAddress);
		}

		if (firstFreeAddress != 0) {
			codeGen.genera(PUSHIMM, firstFreeAddress - 1);
			codeGen.genera(MOVESP);
		}
	}

	@Override
	public void visit(LoopInstruction node) {
		int condAddr = codeGen.indirizzoProssimaIstruzione();
		node.cond.visit(this);
		int jumpEnd = codeGen.generaParziale(JZERO);

		for (Instruction instr : node.body)
			instr.visit(this);
		codeGen.genera(JUMP, condAddr);
		codeGen.completaIstruzione(jumpEnd, codeGen.indirizzoProssimaIstruzione());
	}

	@Override
	public void visit(AssignmentInstruction node) {
		node.expr.visit(this);
		codeGen.genera(POP, table.get(node.ident).getAddress());
	}

	@Override
	public void visit(AssignmentExpr node) {
		visit(new AssignmentInstruction(node.ident, node.expr));
		codeGen.genera(PUSH, table.get(node.ident).getAddress());
	}

	@Override
	public void visit(OutputInstruction node) {
		if (node.message != null) {
			for (char c : node.message.toCharArray()) {
				codeGen.genera(PUSHIMM, c);
				codeGen.genera(OUTPUTCH);
			}
		}

		if (node.expr != null) {
			node.expr.visit(this);
			codeGen.genera(OUTPUT);
		}
	}

	@Override
	public void visit(AddExpr node) {
		node.left.visit(this);
		node.right.visit(this);
		codeGen.genera(ADD);
	}

	@Override
	public void visit(DivExpr node) {
		node.left.visit(this);
		node.right.visit(this);
		codeGen.genera(DIV);
	}

	@Override
	public void visit(InputExpr node) {
		visit(new OutputInstruction(node.message));
		codeGen.genera(INPUT);
	}

	@Override
	public void visit(IntExpr node) {
		codeGen.genera(PUSHIMM, node.value);
	}

	@Override
	public void visit(ModExpr node) {
		throw new RuntimeException("Platform does not natively support remainder");
	}

	@Override
	public void visit(MulExpr node) {
		node.left.visit(this);
		node.right.visit(this);
		codeGen.genera(MUL);
	}

	@Override
	public void visit(NegExpr node) {
		codeGen.genera(PUSHIMM, 0);
		node.expr.visit(this);
		codeGen.genera(SUB);
	}

	@Override
	public void visit(SubExpr node) {
		node.left.visit(this);
		node.right.visit(this);
		codeGen.genera(SUB);
	}

	@Override
	public void visit(TernaryExpr node) {
		node.cond.visit(this);
		int jumpCond = codeGen.generaParziale(JZERO);
		node.ethen.visit(this);
		int jumpEnd = codeGen.generaParziale(JUMP);
		codeGen.completaIstruzione(jumpCond, codeGen.indirizzoProssimaIstruzione());
		node.eelse.visit(this);
		codeGen.completaIstruzione(jumpEnd, codeGen.indirizzoProssimaIstruzione());
	}

	@Override
	public void visit(VarExpr node) {
		codeGen.genera(PUSH, table.get(node.ident).getAddress());
	}

	@Override
	public void visit(EmptyInstruction node) {
		/* do nothing */
	}
}
