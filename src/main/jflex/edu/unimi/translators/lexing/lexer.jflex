package edu.unimi.translators.lexing;

import edu.unimi.translators.diagnostics.Diagnostics;

import java.io.Reader;
import java_cup.runtime.Symbol;
import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.ComplexSymbolFactory.Location;

import static edu.unimi.translators.parsing.CompilerParserSym.*;

%%

%public
%class CompilerLexer

%cup

%char
%line
%column

%{
	private Diagnostics diagnostics;
	private ComplexSymbolFactory symbolFactory;
	private StringBuilder string;

    public CompilerLexer(Reader in, ComplexSymbolFactory sf, Diagnostics diagnostics) {
        this(in);
        this.symbolFactory = sf;
        this.string = new StringBuilder();
        this.diagnostics = diagnostics;
    }

    public CompilerLexer(Reader in, ComplexSymbolFactory sf) {
        this(in, sf, null);
    }

    private Symbol makeSymbol(String name, int id) {
        Location start = new Location(yyline + 1, yycolumn + 1, yychar);
        Location end = new Location(yyline + 1, yycolumn + yylength(), yychar + yylength());
        return symbolFactory.newSymbol(name, id, start, end);
    }

    private Symbol makeSymbol(String name, int id, Object value) {
        Location start = new Location(yyline + 1, yycolumn + 1, yychar);
        Location end = new Location(yyline + 1, yycolumn + yylength(), yychar + yylength());
        return symbolFactory.newSymbol(name, id, start, end, value);
    }

    private Symbol makeSymbol(String name, int sym, Object value, int length) {
        Location start = new Location(yyline + 1, yycolumn + yylength() - length, yychar + yylength() - length);
        Location end = new Location(yyline + 1, yycolumn + yylength(), yychar + yylength());
        return symbolFactory.newSymbol(name, sym, start, end, value);
    }

    private void error(String erroneousInput) {
        if (diagnostics != null)
            System.err.println(diagnostics.createLexingErrorMessage(erroneousInput, yyline + 1, yycolumn + 1));
    }
%}


IDENT = [:jletter:][:jletterdigit:]*
INT_LIT = 0 | [1-9][:digit:]*
HEX_LIT = 0 [xX] [0-9a-fA-F]+
STRING_CHAR = [^\n\r\"\\]
NEWLINE = \r | \n | \r\n | \n\r
SPACES = [ \t\f]

%state STRING_LIT
%state NEWLINE_CONT
%state LINE_COMMENT

%%

<YYINITIAL> {
    \" {
	    yybegin(STRING_LIT);
	    string.setLength(0); // clear the buffer
    }

    "//" {
	    yybegin(LINE_COMMENT);
	    return makeSymbol("NEWLINE", NEWLINE);
    }

    "&" { yybegin(NEWLINE_CONT); }

    /** keywords **/
	"input"   { return makeSymbol("INPUT", INPUT);               }
	"output"  { return makeSymbol("OUTPUT", OUTPUT);             }
	"newLine" { return makeSymbol("STMT_NEWLINE", STMT_NEWLINE); }
	"loop"    { return makeSymbol("LOOP", LOOP);                 }
	"endLoop" { return makeSymbol("END_LOOP", END_LOOP);         }

    /** operators **/
    "+" { return makeSymbol("PLUS", PLUS); }
    "-" { return makeSymbol("MINUS", MINUS); }
    "*" { return makeSymbol("TIMES", TIMES); }
    "/" { return makeSymbol("DIV", DIV); }
    "%" { return makeSymbol("MOD", MOD); }
    "(" { return makeSymbol("LPAREN", LPAREN); }
    ")" { return makeSymbol("RPAREN", RPAREN); }
    "=" { return makeSymbol("EQUAL", EQUAL); }
    "?" { return makeSymbol("QUESTION_MARK", QUESTION_MARK); }
    ":" { return makeSymbol("COLON", COLON); }

	{NEWLINE} { return makeSymbol("NEWLINE", NEWLINE); }

    {INT_LIT} {
        return makeSymbol("NUM", NUM, Integer.parseInt(yytext()));
    }

    {HEX_LIT} {
        return makeSymbol("NUM", NUM, Integer.parseInt(yytext().substring(2), 16));
    }

    {IDENT} { return makeSymbol("IDENT", IDENT, yytext()); }
}

<STRING_LIT> {
    \" {
	    yybegin(YYINITIAL);
	    return makeSymbol("STRING", STRING, string.toString(), string.length());
    }

    \\b  { string.append('\b'); }
    \\f  { string.append('\f'); }
    \\n  { string.append('\n'); }
    \\r  { string.append('\r'); }
    \\t  { string.append('\t'); }
    \\\" { string.append('\"'); }
    \\'  { string.append('\''); }
    \\\\ { string.append('\\'); }

    {STRING_CHAR}+ { string.append(yytext()); }
}

<NEWLINE_CONT> {
    {NEWLINE} { yybegin(YYINITIAL); }
    "//" { yybegin(LINE_COMMENT); }
}

<LINE_COMMENT> {
    {NEWLINE} { yybegin(YYINITIAL); }
    . { /* ignore */ }
}

{SPACES} { /* ignore */ }

/* catch-all rule */
. {
	error(yytext());
	return makeSymbol("error", error);
}

<<EOF>>  {
	return makeSymbol("EOF", EOF);
}
