package edu.unimi.translators;

import edu.unimi.translators.ast.instructions.Program;
import edu.unimi.translators.codegen.CodeGenerator;
import edu.unimi.translators.diagnostics.Diagnostics;
import edu.unimi.translators.lexing.CompilerLexer;
import edu.unimi.translators.parsing.CompilerParser;
import java_cup.runtime.ComplexSymbolFactory;
import lt.macchina.Codice;
import lt.macchina.Macchina;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

public class TestUtils {
	public static Program parse(String input) throws Exception {
		return parse(new StringReader(input), null);
	}

	public static Program parse(Reader input, Diagnostics diagnostics) throws Exception {
		ComplexSymbolFactory symbolFactory = new ComplexSymbolFactory();
		CompilerParser parser = new CompilerParser(new CompilerLexer(input, symbolFactory, diagnostics), symbolFactory);
		parser.diagnostics = diagnostics;
		return (Program) parser.parse().value;
	}

	public static String gencode(String source) throws Exception {
		return gencode(new StringReader(source));
	}

	public static String gencode(Reader input) throws Exception {
		ComplexSymbolFactory symbolFactory = new ComplexSymbolFactory();
		CompilerLexer lexer = new CompilerLexer(input, symbolFactory);
		CompilerParser parser = new CompilerParser(lexer, symbolFactory);

		Program prog = (Program) parser.parse().value;
		File tempFile = getTempFile();
		new CodeGenerator(parser.getSymbolTable(), new Codice(tempFile.getAbsolutePath())).gencode(prog);
		return new Macchina(tempFile.getAbsolutePath()).toString();
	}

	public static File getTempFile() throws IOException {
		File tempFile = File.createTempFile("test", "out");
		tempFile.deleteOnExit();
		return tempFile;
	}

	public static InputStream getTestCase(String testCase) {
		return TestUtils.class.getResourceAsStream(testCase);
	}
}
