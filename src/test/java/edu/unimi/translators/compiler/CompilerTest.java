package edu.unimi.translators.compiler;

import lt.macchina.Macchina;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import static edu.unimi.translators.TestUtils.getTempFile;
import static edu.unimi.translators.TestUtils.getTestCase;
import static java.lang.System.lineSeparator;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;

public class CompilerTest {
	@ParameterizedTest
	@ValueSource(strings = {
		"even-odd",
		"gcd",
		"min",
		"mult"
	})
	void testCompile(String testcase) throws Exception {
		Compiler compiler = new Compiler(getTestCase("/tests/provided/" + testcase + ".prg"));
		File output = getTempFile();
		compiler.compile(output.getAbsolutePath());

		assertLinesMatch(
			getAllLines(getTestCase("/tests/provided/" + testcase + ".asm")),
			getAssemblyCode(output)
		);
	}

	@Test
	void testLoopAndTernary() throws Exception {
		InputStream istream = new ByteArrayInputStream((
			"x = 9\n" +
				"loop x ? 0 : 1\n" +
				"\tx = x - 1\n" +
				"\toutput x\n" +
				"\tnewLine\n" +
				"endLoop\n").getBytes(Charset.defaultCharset()));
		Compiler compiler = new Compiler(istream);
		File output = getTempFile();
		compiler.compile(output.getAbsolutePath());

		assertLinesMatch(asList(
			"  0 PUSH=  0",
			"  2 MOVESP",
			"  3 PUSH=  9", // x = 9
			"  5 POP    0",
			"  7 PUSH   0", // x
			"  9 JZERO  15",// ?
			" 11 PUSH=  0", // 0
			" 13 JUMP   17",// :
			" 15 PUSH=  1", // 1
			" 17 JZERO  34",// loop
			" 19 PUSH   0", // x - 1
			" 21 PUSH=  1",
			" 23 SUB",
			" 24 POP    0", // x = x - 1
			" 26 PUSH   0", // output x
			" 28 OUTPUT",
			" 29 PUSH=  10",// newLine
			" 31 OUTPUTCH",
			" 32 JUMP   7", // endLoop
			" 34 HALT"
			),
			getAssemblyCode(output)
		);
	}

	private List<String> getAllLines(InputStream input) {
		return new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8)).lines().collect(Collectors.toList());
	}

	private List<String> getAssemblyCode(File output) throws IOException {
		return asList(new Macchina(output.getAbsolutePath())
			.toString()
			.split(lineSeparator()));
	}
}
