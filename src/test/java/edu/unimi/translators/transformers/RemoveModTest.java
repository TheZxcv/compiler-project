package edu.unimi.translators.transformers;

import edu.unimi.translators.ast.instructions.Program;
import edu.unimi.translators.compiler.SymbolTable;
import org.junit.jupiter.api.Test;

import static edu.unimi.translators.TestUtils.parse;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RemoveModTest {
	@Test
	void testModInstructionRemoval() throws Exception {
		Program actual = parse("output input % input\n");
		Program expected = parse("output (r0 = input) - (r0 / (r0 = input)) * r0\n");
		RemoveMod transformer = new RemoveMod(new SymbolTable(), "r0");

		assertEquals(expected, transformer.transform(actual));
	}

	@Test
	void testLeftUnchanged() throws Exception {
		Program initial = parse("output input + input\n");
		RemoveMod transformer = new RemoveMod(new SymbolTable());

		assertEquals(initial, transformer.transform(initial));
	}
}
