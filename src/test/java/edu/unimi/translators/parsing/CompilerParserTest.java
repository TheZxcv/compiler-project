package edu.unimi.translators.parsing;

import edu.unimi.translators.ast.instructions.Program;
import edu.unimi.translators.lexing.CompilerLexer;
import java_cup.runtime.ComplexSymbolFactory;
import org.junit.jupiter.api.Test;

import java.io.StringReader;

import static edu.unimi.translators.TestUtils.parse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class CompilerParserTest {
	@Test
	public void testTernaryAssociativity() throws Exception {
		Program ambiguous = parse("x = 1 ? 2 : 3 ? 4 : 5\n");
		Program correct = parse("x = 1 ? 2 : (3 ? 4 : 5)\n");
		Program incorrect = parse("x = (1 ? 2 : 3) ? 4 : 5\n");

		assertEquals(correct, ambiguous);
		assertNotEquals(incorrect, ambiguous);
	}

	@Test
	public void testAdditivePrecedence() throws Exception {
		Program actual = parse("x = 1 + 2 - 3\n");
		Program correct = parse("x = (1 + 2) - 3\n");
		Program incorrect = parse("x = 1 + (2 - 3)\n");

		assertEquals(correct, actual);
		assertNotEquals(incorrect, actual);
	}

	@Test
	public void testMultiplicativePrecedence() throws Exception {
		Program actual = parse("x = 1 * 2 / 3\n");
		Program correct = parse("x = (1 * 2) / 3\n");
		Program incorrect = parse("x = 1 * (2 / 3)\n");

		assertEquals(correct, actual);
		assertNotEquals(incorrect, actual);

		actual = parse("x = 2 * 2 % 2\n");
		correct = parse("x = (2 * 2) % 2\n");
		incorrect = parse("x = 2 * (2 % 2)\n");

		assertEquals(correct, actual);
		assertNotEquals(incorrect, actual);
	}

	@Test
	public void testAdditiveTernaryPrecedence() throws Exception {
		Program actual = parse("x = 1 + 1 ? 2 : 3\n");
		Program correct = parse("x = (1 + 1) ? 2 : 3\n");
		Program incorrect = parse("x = 1 + (1 ? 2 : 3)\n");

		assertEquals(correct, actual);
		assertNotEquals(incorrect, actual);
	}

	@Test
	public void testEmptyInstructionShouldParse() throws Exception {
		parse("\n");
	}

}
