package edu.unimi.translators.parsing.diagnostics;

import edu.unimi.translators.buffer.LineBufferedInputStream;
import edu.unimi.translators.diagnostics.Diagnostics;
import edu.unimi.translators.lexing.CompilerLexer;
import edu.unimi.translators.parsing.CompilerParser;
import java_cup.runtime.ComplexSymbolFactory;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import static edu.unimi.translators.TestUtils.getTestCase;
import static java.lang.System.lineSeparator;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;

public class ParsingDiagnosticsTest {
	@Test
	void testMissingCondition() throws Exception {
		LineBufferedInputStream buffer = new LineBufferedInputStream(getTestCase("/tests/missing-cond.prg"));
		InputStreamReader reader = new InputStreamReader(buffer);
		assertMatchingOutput(
			parserOutputMessages(reader, new Diagnostics(buffer)),
			"Syntax error (line=1, col=5) got NEWLINE",
			"line 1: loop",
			"            ^"
		);
	}

	@Test
	void testMissingNewline() throws Exception {
		LineBufferedInputStream buffer = new LineBufferedInputStream(getTestCase("/tests/missing-newline.prg"));
		InputStreamReader reader = new InputStreamReader(buffer);
		assertMatchingOutput(
			parserOutputMessages(reader, new Diagnostics(buffer)),
			"Syntax error (line=1, col=10) got OUTPUT",
			"line 1: output 1 output \"a\"",
			"                 ^"
		);
	}

	@Test
	void testMissingOperator() throws Exception {
		LineBufferedInputStream buffer = new LineBufferedInputStream(getTestCase("/tests/missing-operator.prg"));
		InputStreamReader reader = new InputStreamReader(buffer);
		assertMatchingOutput(
			parserOutputMessages(reader, new Diagnostics(buffer)),
			"Syntax error (line=1, col=7) got NUM",
			"line 1: x = 1 1",
			"              ^"
		);
	}

	@Test
	void testMissingColon() throws Exception {
		LineBufferedInputStream buffer = new LineBufferedInputStream(getTestCase("/tests/missing-colon.prg"));
		InputStreamReader reader = new InputStreamReader(buffer);
		assertMatchingOutput(
			parserOutputMessages(reader, new Diagnostics(buffer)),
			"Syntax error (line=1, col=11) got NUM",
			"line 1: x = 1 ? 2 1",
			"                  ^"
		);
	}

	@Test
	void testMissingNewlineBeforeEndloop() throws Exception {
		LineBufferedInputStream buffer = new LineBufferedInputStream(getTestCase("/tests/missing-newline-before-endloop.prg"));
		InputStreamReader reader = new InputStreamReader(buffer);
		assertMatchingOutput(
			parserOutputMessages(reader, new Diagnostics(buffer)),
			"Syntax error (line=2, col=10) got END_LOOP",
			"line 2:     newLine endLoop",
			"                    ^"
		);
	}

	private static void assertMatchingOutput(String actual, String... expected) {
		List<String> outputLines = asList(actual.split(lineSeparator()));
		assertLinesMatch(asList(expected), outputLines.subList(0, expected.length));
	}

	private static String parserOutputMessages(Reader input, Diagnostics diagnostics) throws UnsupportedEncodingException {
		ComplexSymbolFactory symbolFactory = new ComplexSymbolFactory();
		CompilerParser parser = new CompilerParser(new CompilerLexer(input, symbolFactory), symbolFactory);
		parser.diagnostics = diagnostics;
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		parser.logger = new PrintStream(output);
		try {
			parser.parse();
		} catch (Exception e) { }
		return output.toString("UTF-8");
	}
}
