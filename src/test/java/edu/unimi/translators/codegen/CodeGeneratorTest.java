package edu.unimi.translators.codegen;

import edu.unimi.translators.TestUtils;
import org.junit.jupiter.api.Test;

import static edu.unimi.translators.TestUtils.gencode;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CodeGeneratorTest {

	@Test
	void testEmpty() throws Exception {
		String assembled = TestUtils.gencode("\n");

		assertEquals(" 0 HALT\n", assembled);
	}

	@Test
	void testOutput() throws Exception {
		String assembled = TestUtils.gencode("output 1\n");

		assertEquals(" 0 PUSH=  1\n" +
			" 2 OUTPUT\n" +
			" 3 HALT\n", assembled);
	}

	@Test
	void testAdd() throws Exception {
		String assembled = TestUtils.gencode("output 1 + 1\n");

		assertEquals(" 0 PUSH=  1\n" +
			" 2 PUSH=  1\n" +
			" 4 ADD\n" +
			" 5 OUTPUT\n" +
			" 6 HALT\n", assembled);
	}

	@Test
	void testTernary() throws Exception {
		String assembled = TestUtils.gencode("output 1 ? 2 : 3\n");

		assertEquals("  0 PUSH=  1\n" +
			"  2 JZERO  8\n" +
			"  4 PUSH=  2\n" +
			"  6 JUMP   10\n"+
			"  8 PUSH=  3\n" +
			" 10 OUTPUT\n" +
			" 11 HALT\n", assembled);
	}

	@Test
	void testNoMod() throws Exception {
		assertThrows(RuntimeException.class,
			() -> TestUtils.gencode("x = 1 % 2\n")
		);
	}

}
