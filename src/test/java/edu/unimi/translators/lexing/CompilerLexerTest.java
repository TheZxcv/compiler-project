package edu.unimi.translators.lexing;

import java_cup.runtime.ComplexSymbolFactory;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;

import static edu.unimi.translators.parsing.CompilerParserSym.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompilerLexerTest {

	CompilerLexer lexer;

	@Test
	void testIntegerLiteral() throws IOException {
		lexer = createLexer("123456");
		assertEquals(123456, (int) lexer.next_token().value);
	}

	@Test
	void testHexLiteral() throws IOException {
		lexer = createLexer("0x1ff1");
		assertEquals(0x1ff1, (int) lexer.next_token().value);
	}

	@Test
	void testStringLiteral() throws IOException {
		lexer = createLexer("\"literal!\\\"?\"");
		assertEquals("literal!\"?", lexer.next_token().value);
	}

	@Test
	void testIdentifier() throws IOException {
		lexer = createLexer("varName1");
		assertEquals("varName1", lexer.next_token().value);
	}

	@Test
	void testIdentifierInvalid() throws IOException {
		lexer = createLexer("1varName");
		assertEquals(1, lexer.next_token().value);
		assertEquals("varName", lexer.next_token().value);
	}

	@Test
	void testNewLine() throws IOException {
		lexer = createLexer("\n");
		assertEquals(NEWLINE, lexer.next_token().sym);
	}

	@Test
	void testSkipNewLine() throws IOException {
		lexer = createLexer("1&     \nident\n");
		assertEquals(NUM, lexer.next_token().sym);
		assertEquals(IDENT, lexer.next_token().sym);
		assertEquals(NEWLINE, lexer.next_token().sym);
		assertEquals(EOF, lexer.next_token().sym);
	}

	@Test
	void testStuffBetweenSkipNewLineAndNewLine() throws IOException {
		lexer = createLexer("1&   ident\n");
		assertEquals(NUM, lexer.next_token().sym);
		assertEquals(error, lexer.next_token().sym);
	}

	@Test
	void testKeyword() throws IOException {
		lexer = createLexer("input");
		assertEquals(INPUT, lexer.next_token().sym);
	}

	@Test
	void testCorrectSnippet() throws IOException {
		lexer = createLexer("//calcolo del resto" + System.lineSeparator()
			+ "resto = &  //segue divisione intera" + System.lineSeparator()
			+ "x / y" + System.lineSeparator());
		assertEquals(NEWLINE, lexer.next_token().sym);
		assertEquals(IDENT, lexer.next_token().sym);
		assertEquals(EQUAL, lexer.next_token().sym);
		assertEquals(IDENT, lexer.next_token().sym);
		assertEquals(DIV, lexer.next_token().sym);
		assertEquals(IDENT, lexer.next_token().sym);
		assertEquals(NEWLINE, lexer.next_token().sym);
		assertEquals(EOF, lexer.next_token().sym);
	}

	@Test
	void testIncorrectSnippet() throws IOException {
		lexer = createLexer("//calcolo del resto " + System.lineSeparator()
			+ "resto = &" + System.lineSeparator()
			+ "//segue divisione intera &" + System.lineSeparator()
			+ "x / y" + System.lineSeparator());
		assertEquals(NEWLINE, lexer.next_token().sym);
		assertEquals(IDENT, lexer.next_token().sym);
		assertEquals(EQUAL, lexer.next_token().sym);
		assertEquals(NEWLINE, lexer.next_token().sym);
		assertEquals(IDENT, lexer.next_token().sym);
		assertEquals(DIV, lexer.next_token().sym);
		assertEquals(IDENT, lexer.next_token().sym);
		assertEquals(NEWLINE, lexer.next_token().sym);
		assertEquals(EOF, lexer.next_token().sym);
	}

	private static CompilerLexer createLexer(String input) {
		return new CompilerLexer(new StringReader(input), new ComplexSymbolFactory());
	}
}
