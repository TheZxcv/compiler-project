package edu.unimi.translators.analysis;

import edu.unimi.translators.ast.instructions.Program;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static edu.unimi.translators.TestUtils.parse;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UninitializedVarAnalysisTest {

	private UninitializedVarAnalysis analysis;

	@BeforeEach
	void setUp() {
		analysis = new UninitializedVarAnalysis();
	}

	@Test
	void testUninitializedDetection() throws Exception {
		Program prog = parse("output i\n");
		assertFalse(analysis.analyze(prog));
	}

	@Test
	void testNoUninitializedVariable() throws Exception {
		Program prog = parse(
			"i = 0\n" +
				"output i\n"
		);
		assertTrue(analysis.analyze(prog));
	}

	@Test
	void testVariableInitializedInsideLoop() throws Exception {
		Program prog = parse(
			"loop 0\n" +
				"x = 0\n" +
				"endLoop\n" +
				"output x\n"
		);
		assertFalse(analysis.analyze(prog));
	}
}
